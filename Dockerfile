FROM python:3

RUN mkdir app

COPY ./requirements.txt ./app

RUN pip3 install --no-cache-dir -r ./app/requirements.txt

COPY ./ ./app

WORKDIR /app

ENTRYPOINT ["python3", "main.py"]
