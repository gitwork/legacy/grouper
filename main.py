#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Программа для обработки групп на сервисе gitwork.ru
"""

from functools import wraps
import argparse
import logging
import warnings as w
import sys
import requests

import gitlab
from flask import Flask, render_template, request, flash, url_for, redirect, session

from scripts import methods, args
from scripts.gitlab_connection import Connection
app = Flask(__name__)
app.config.from_json('microservice_cfg.json')
app.secret_key = '0adeaf40-1196-49b4-8b13-7d9cc68b005c'


# pylint: disable=missing-function-docstring, broad-except, redefined-outer-name, lost-exception
# pylint: disable=broad-except
# Отключено, т.к. ловим любую ошибку и выводим сообщение о ней

# Запускается через 127.0.0.1/grouper/login (Правило sms-приложения)
@app.route('/grouper/')
def begin():
    """ start """
    return render_template("home.html")


# pylint: disable=invalid-name
def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        flash('Авторизуйся!', 'danger')
        return redirect(url_for('login'))
    return wrap
# pylint: enable=invalid-name


@app.route('/grouper/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':

        token = request.form['token']
        try:

            git.set_token(token)
            git.set_gitlab(SITE)

            session['logged_in'] = True
            msg = 'Успешный вход'
            return render_template('home.html', msg=msg)
        except gitlab.GitlabAuthenticationError:
            error = 'Неверный токен'
            return render_template('login.html', error=error)
        except requests.exceptions.RequestException:
            error = 'Нет соединения с git'
            return render_template('login.html', error=error)
    return render_template('login.html')


@app.route('/grouper/logout/')
@is_logged_in
def logout():
    session.clear()
    flash('Успешный выход', 'success')
    return redirect(url_for('begin'))


@app.route('/grouper/meth/group_add/', methods=['GET', 'POST'])
@is_logged_in
def group_add():
    """
    Returns: возвращает на страницу с выводом состояния
    Raises:
        FileNotFoundError - ошибка поиска файла
        ValueError - неправильно названа группа/подгруппа
    """
    if request.method == 'GET':
        return render_template("group_add.html")

    group = request.form.get("group_name")
    subgroup = request.form.get("subgroup_name")
    file_prepods = request.form.get("file_prepods")
    file_users = request.form.get("file_users")
    secured = request.form.get("secured")
    milestone = request.form.get("milestone")
    kidp = request.form.get("kidp")
    result = request.form.get("result")

    try:
        git.set_add_g(group, file_prepods, file_users, subgroup)
        git.set_add_o(milestone, kidp, result)
        methods.check_secure(secured, group)
    except UserWarning as err:
        flash(str(err), category='danger')
        return redirect(url_for('group_add'))
    except FileNotFoundError as err:
        flash(str(err), category='danger')
        return redirect(url_for('group_add'))
    except ValueError as err:
        flash(str(err), category='danger')
        return redirect(url_for('group_add'))

    try:
        with w.catch_warnings(record=True) as warnings:
            w.simplefilter("always")
            methods.create_group_and_proj(git)
    except Warning as err:
        flash(str(err), category='success')
    except gitlab.GitlabSearchError as err:
        flash(str(err), category='danger')
    except ValueError as err:
        flash(str(err), category='danger')
        return redirect(url_for('group_add'))
    finally:
        if warnings:
            for out in warnings:
                flash(str(out.message), category='danger')
        return redirect(url_for('group_add'))


@app.route('/grouper/meth/sub_add/', methods=['GET', 'POST'])
@is_logged_in
def sub_add():
    """
        Returns: возвращает на страницу с выводом состояния
        Raises:
            FileNotFoundError - ошибка поиска файла
    """
    if request.method == 'GET':
        return render_template("sub_add.html")

    sub_name = request.form.get("sub_name")
    branch = request.form.get("branch")
    file_prepods = request.form.get("file_prepods")
    secured = request.form.get("secured")

    try:
        git.set_add_s(sub_name, branch, file_prepods)
        methods.check_secure(secured, sub_name)
    except FileNotFoundError as err:
        flash(str(err), category='danger')
        return redirect(url_for('sub_add'))
    except ValueError as err:
        flash(str(err), category='danger')
        return redirect(url_for('sub_add'))
    except UserWarning as err:
        flash(str(err), category='danger')
        return redirect(url_for('sub_add'))

    try:
        with w.catch_warnings(record=True) as warnings:
            w.simplefilter("always")
            methods.create_sub(git)

    except UserWarning as err:
        flash(str(err), category='danger')
    except Warning as check:
        flash(str(check), category='success')
    except Exception as err:
        flash(str(err), category='danger')
    finally:
        if warnings:
            for out in warnings:
                flash(str(out.message), category='danger')
        return redirect(url_for('sub_add'))


@app.route('/grouper/meth/group_del/', methods=['GET', 'POST'])
@is_logged_in
def group_del():
    """
    Удаление группы
    """

    if request.method == 'GET':
        return render_template("group_del.html")

    group_name = request.form.get("group_name")
    subgroup_name = request.form.get("subgroup_name")

    try:
        git.set_del_g(group_name, subgroup_name)
    except ValueError as err:
        flash(str(err), category='danger')
        return redirect(url_for('group_del'))

    try:
        methods.delete_group(git)

    except Warning as err:
        flash(str(err), category='success')
    except Exception as err:
        flash(str(err), category='danger')
    finally:
        return redirect(url_for('group_del'))


@app.route('/grouper/meth/group_delm/', methods=['GET', 'POST'])
@is_logged_in
def group_delm():
    """
    Удаление пользователей из группы
    """
    if request.method == 'GET':
        return render_template("group_delm.html")

    group = request.form.get("name_group")
    file_users = request.form.get("file_users")

    try:
        git.set_del_m(group, file_users)
    except FileNotFoundError as err:
        flash(str(err), category='danger')
        return redirect(url_for('group_delm'))
    except ValueError as err:
        flash(str(err), category='danger')
        return redirect(url_for('group_delm'))

    try:
        with w.catch_warnings(record=True) as warnings:
            w.simplefilter("always")
            methods.delete_members(git)
    except Warning as check:
        flash(str(check), category='success')
    finally:
        if warnings:
            for out in warnings:
                flash(str(out.message), category='danger')
        return redirect(url_for('group_delm'))


if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)
    PARSER = argparse.ArgumentParser(description='GitWork_Create_group_and_projects')
    ARGS = args.parse_args(PARSER)

    GROUP = ARGS.name
    SUBGROUP = ARGS.subname
    TOKEN = ARGS.token
    GUARD = ARGS.guarded
    KIDP = ARGS.kidp
    RESULTS = ARGS.results
    MILESTONE = ARGS.milestone
    SITE = ARGS.site
    ID = ARGS.id
    FILE_USERS = ARGS.users
    FILE_PREPODS = ARGS.prepods
    # pylint: disable=broad-except
    # Отключено, т.к. ловим любую ошибку и выводим сообщение о ней
    try:
        git = Connection()
        with w.catch_warnings(record=True) as warnings:
            w.simplefilter("always")
            if TOKEN:
                git.set_token(TOKEN)
                git.set_gitlab(SITE)

                if ARGS.group_add and TOKEN:
                    if ARGS.guarded:
                        methods.protect_group(GROUP)

                    git.set_add_g(GROUP, FILE_PREPODS, FILE_USERS, SUBGROUP)
                    git.set_add_o(MILESTONE, KIDP, RESULTS)
                    methods.create_group_and_proj(git)

                if ARGS.group_del:
                    git.set_del_g(GROUP, SUBGROUP)
                    methods.delete_group(git)

                if ARGS.sub_add:
                    git.set_add_s(SUBGROUP, GROUP, FILE_PREPODS)
                    methods.create_sub(git)

                if ARGS.group_delm:
                    git.set_del_m(GROUP, FILE_USERS, SUBGROUP)
                    methods.delete_members(git)

    except FileNotFoundError as err:
        sys.exit(err)
    except ValueError as err:
        sys.exit(err)
    except UserWarning as err:
        sys.exit(err)
    except gitlab.GitlabAuthenticationError as err:
        sys.exit(err)
    except Warning as err:
        if warnings:
            for out in warnings:
                print(str(out.message))
        sys.exit(err)
    except Exception as err:
        if warnings:
            for out in warnings:
                print(str(out.message))
        sys.exit(err)
    else:
        git = Connection()
        # http://127.0.0.1:5000/grouper/login
        # 0.0.0.0 для docker
        app.run(host='0.0.0.0')
