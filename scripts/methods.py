"""
Здесь описаны функции, используемые в программе Grouper

Methods:
    check_group           - проверка группы на защищенность
    protect_group         - занесение группы в файл для защиты от изменения
    create_group_and_proj - создание групп/проектов
    delete_group          - удаление групп/подгрупп
    create_sub            - создание предмета
    delete_members        - удаление пользователей
"""

import warnings
import pandas as pd

import gitlab

# Права доступа для сервиса git
GUEST = gitlab.GUEST_ACCESS
OWNER = gitlab.OWNER_ACCESS
DEVELOPER = gitlab.DEVELOPER_ACCESS


# ----------------------------------------------

def check_secure(secured, group):
    """
    Проверка на защиту
    Args:
        secured: бит защиты
        group:   название группы

    Returns: None

    """
    if secured is None:
        raise UserWarning('Вы не выбрали параметр защищенности группы')

    if secured is True:
        protect_group(group)


def check_group(name):
    """
    Проверка на присутствие группы в файле защищенных групп
    Args:
        name (str):   Название группы

    Returns:
        True:   Название присутствует
        False:  Название отсутствует

    """
    try:
        file = pd.read_csv("./data/groups.csv", sep=";")
        res = file[file['groups'].isin([name])]
        if res.empty:
            return False
    except pd.errors.EmptyDataError:
        return False
    return True


def protect_group(name):
    # pylint: disable=invalid-name
    """
    Защита группы от удаления
    Args:
        name (str): название защищаемой группы

    Returns:
        True

    """
    data = [name]

    df = pd.DataFrame(data)
    df.to_csv(r'./data/groups.csv', mode='ab', header=False, index=False)
    return True


def remove_rudiments(groups, group_name):
    """
    Удаляем ненужные группы вида 'group_name / какой-то треш'
    Args:
        groups:     list групп
        group_name: название необходимой группы

    Returns:
        list очищенных групп

    """
    result = []
    for i in groups[:]:
        if i.name == group_name.rpartition('/ ')[2]:
            result.append(i)
    return result


def check_len(groups):
    """
    Проверка на количество групп
    Args:
        groups: list групп

    Returns:
        None

    """
    if len(groups) != 1:
        string = '\n'
        names = [f"{i.full_name} ; " for i in groups]
        result = string.join(names)
        raise ValueError(f"Количество групп больше одной. "
                         f"Введите полный путь в формате 'group / subgroup'."
                         f" Найденные группы: {result}")


# ----------------------------------------------
# Create
def create_group_and_proj(git_ob):
    """ Создание групп
    В файле должен быть столбец login, который содержит логины преподавателей.
    Каждый преподаватель будет добавлен в группу с правами "owner".
    В файле должен быть столбец login и fullname,
    которые содержат логины слушателей и их полные имена.
    Для каждого слушателя будет создан проект с названием <fullname>,
    он будет добавлен в этот проект с правами "developer":
        Args:
            git_ob - объект для работы с сервисом git

        Returns:
            Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.

        Raises:
            gitlab.GitlabAuthenticationError
            gitlab.exceptions.GitlabCreateError
            gitlab.GitlabSearchError
            Warning - вызывается в случае успешного выполнения

    """

    # Получение необходимых элементов
    git = git_ob.ret_git()
    group_name = git_ob.ret_group_name()

    # Работа с группой
    if git_ob.result_group() is False:
        try:
            git.groups.create({'name': group_name, 'path': group_name})
        except gitlab.exceptions.GitlabCreateError as err:
            print(err)
            warnings.warn(f"Ошибка создания группы {group_name}. Если нет успешного выполнения,"
                          f" создайте вручную и вызовите повторно функцию.", ResourceWarning)

    group_name = git_ob.ret_group_name()
    subgroup_name = git_ob.ret_subgroup_name()
    groups = git.groups.list(search=group_name)

    # Удалим вложенные группы, проверим количество подходящих
    groups = remove_rudiments(groups, group_name)
    check_len(groups)

    group = groups[0]

    git_ob.set_group(group)
    git_ob.add_prepods()

    if not subgroup_name:
        git_ob.create_proj()
        raise Warning("Успешное выполнение")

    # Работа с подгруппой
    try:
        git.groups.create({'name': subgroup_name, 'path': subgroup_name,
                           'parent_id': group.id})
    except gitlab.exceptions.GitlabCreateError as err:
        print(err)
        warnings.warn(f"Подгруппа {subgroup_name} уже существует", ResourceWarning)

    subgroup = git_ob.get_subgroup(group, subgroup_name)
    git_ob.set_group(subgroup)

    git_ob.create_proj()

    raise Warning("Успешное выполнение")


def create_sub(git_ob):
    """ Создание предмета
            Args:
                git_ob - объект для работы с сервисом git

            Returns:
                Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.

            Raises:
                gitlab.GitlabAuthenticationError
                gitlab.exceptions.GitlabCreateError
                gitlab.GitlabSearchError
                Warning - вызывается в случае успешного выполнения

    """

    branch = git_ob.ret_group_name()
    sub_name = git_ob.ret_subgroup_name()
    group = None

    git = git_ob.ret_git()

    # Получение родительского каталог sub/int
    try:

        group = git.groups.list(search=branch, order_by='id', sort='desc')[0]

        git.groups.create({'name': sub_name, 'path': sub_name,
                           'parent_id': group.id})
        group = git.groups.list(search=branch, order_by='id', sort='desc')[0]
    except gitlab.exceptions.GitlabCreateError as err:
        print(err)
        warnings.warn(f"Подгруппа {sub_name} уже существует", ResourceWarning)
    except IndexError as err:
        raise gitlab.GitlabSearchError(f"{branch} не найден") from err

    # Добавление преподавателей в subject
    try:
        real_group = git_ob.get_subgroup(group, sub_name)
        git_ob.set_group(real_group)
        git_ob.add_prepods()
    except gitlab.exceptions.GitlabListError:
        warnings.warn(f"Подгруппа {sub_name} не найдена", ResourceWarning)
    except gitlab.exceptions.GitlabSearchError as err:
        raise gitlab.GitlabSearchError("Ошибка поиска") from err
    raise Warning("Успешное выполнение")


# ----------------------------------------------
# Delete
def delete_group(git_ob):
    """
        Удаление группы или подгруппы
    Args:
        git - объект для работы с сервисом git
    Returns:
        Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.
    Raises:
        SystemError - проверка на защиту
        gitlab.GitlabAuthenticationError
        gitlab.exceptions.GitlabDeleteError
        IndexError - вызывается, если не найдена группа
        Warning - вызывается в случае успешного выполнения

    """
    group_name = git_ob.ret_group_name()
    subgroup_name = git_ob.ret_subgroup_name()
    git = git_ob.set_gitlab()

    if check_group(group_name):
        raise SystemError('Группа защищена')

    try:
        group = git.groups.list(search=group_name, order_by='id')[0]
    except IndexError as err:
        raise IndexError(f"{group_name} не найден") from err

    try:
        if not subgroup_name:
            group.delete()
            raise Warning(f"Группа {group_name} удалена успешно")

        subgroup = group.subgroups.list(search=subgroup_name, order_by='id')[0]
        git.groups.get(subgroup.id, lazy=True).delete()
        raise Warning(f"Группа {subgroup_name} удалена успешно")
    except gitlab.exceptions.GitlabDeleteError as err:
        raise gitlab.GitlabDeleteError("Ошибка удаления группы") from err
    except IndexError as err:
        raise IndexError(f"{subgroup_name} не найден") from err


def delete_members(git_ob):
    """
        Удаление пользователей
    Args:
        git_ob: Объект работы с сервисом git
    Returns:
        Возвращает сообщение об успешности функции, в ином случае сообщение об ошибке.
    Raises:
        SystemError - проверка на защиту
        gitlab.GitlabAuthenticationError
        gitlab.exceptions.GitlabDeleteError
        IndexError - вызывается, если не найдена группа
        Warning - вызывается в случае успешного выполнения

    """
    group_name = git_ob.ret_group_name()
    subgroup_name = git_ob.ret_subgroup_name()
    git = git_ob.ret_git()

    try:
        group = git.groups.list(search=group_name, order_by='id', sort='desc')[0]
    except IndexError as err:
        raise IndexError(f"{group_name} не найден") from err

    if subgroup_name is None:
        git_ob.delete_users(group)
        raise Warning(f"Пользователи удалены успешно из {group_name}")

    try:
        subgroup = git_ob.get_subgroup(group, subgroup_name)
    except IndexError as err:
        raise IndexError(f"{subgroup_name} не найден") from err

    git_ob.delete_users(subgroup)
    raise Warning(f"Пользователи удалены успешно из {subgroup_name}")
