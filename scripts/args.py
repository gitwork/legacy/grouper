"""
Обработка аргументов в cli

Methods:
    parse_args - добавление парсера
"""
from argparse import SUPPRESS

from scripts.gitlab_connection import SITE


def parse_args(parser):
    """

    Args:
        parser: парсер

    Returns: аргументы для парсера

    """
    # pylint: disable=line-too-long
    # Отключил ошибку для читаемости кода без переноса строки
    parser.add_argument('-t', '--token', type=str, help='Input key for work')
    parser.add_argument('-n', '--name', type=str, default=None, help='Input name of main group')
    parser.add_argument('-s', '--subname', type=str, default=None, help='Input name of subgroup')
    parser.add_argument('-u', '--users', type=str, default='./data/1.csv', help='Input dir for users')
    parser.add_argument('-p', '--prepods', type=str, default='./data/2.csv', help='Input dir for prepods')

    parser.add_argument('-g', '--guarded', action='store_true', default=False, help='Add protect for group')
    parser.add_argument('-m', '--milestone', type=str, default=None, help='Add milestones for projects. YYYY-MM-DD')
    parser.add_argument('-k', '--kidp', action='store_true', default=False, help='Make a kidp group')
    parser.add_argument('-r', '--results', action='store_true', default=False, help='Make a group in "results"')
    parser.add_argument('-i', '--inside', type=str, default=0, help='Remove from subproject. Only for group_delm')

    parser.add_argument('--site', type=str, default=SITE, help=SUPPRESS)
    parser.add_argument('--id', type=str, default=None, help=SUPPRESS)

    parser.add_argument('--group_del', action='store_true', help='Delete group')
    parser.add_argument('--group_delm', action='store_true', help='Delete members from group/subgroup')
    parser.add_argument('--group_add', action='store_true', help='Add group. May be secured')
    parser.add_argument('--sub_add', action='store_true', help='Add subject. May be secured')

    return parser.parse_args()
