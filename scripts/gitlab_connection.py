"""
Описан класс работы с сервисом git
"""
import time
import warnings
import pandas as pd
import regex as re

import gitlab

# pylint: disable = redefined-builtin
from requests.exceptions import ConnectionError

GUEST = gitlab.GUEST_ACCESS
OWNER = gitlab.OWNER_ACCESS
DEVELOPER = gitlab.DEVELOPER_ACCESS
SITE = 'https://gitlab.com/'


# pylint: disable=too-many-instance-attributes, missing-function-docstring
class Connection:
    """ Connection class """

    def __init__(self):
        """
        init для класса работы с сервисом git
        """
        self._git = None
        self._token = None
        self._group_name = None
        self._group = None
        self._subgroup = None
        self._subgroup_name = None
        self._file_prepods = None
        self._file_users = None
        self._kidp = None
        self._id = None
        self._milestone = None
        self._result = None

    def ret_group_name(self):
        return self._group_name

    def ret_subgroup_name(self):
        return self._subgroup_name

    def ret_git(self):
        return self._git

    def set_token(self, token):
        self._token = token

    def set_gitlab(self, site, ssl=True):
        """
        Производит авторизацию по токену и возвращает
            объект соединения с сервисом git

            Returns:
                git: объект соединения с сервером
                err: сообщение об ошибке авторизации
        """

        try:
            self._git = gitlab.Gitlab(site, private_token=self._token, ssl_verify=ssl)
            self._git.auth()

        # pylint: disable = redefined-builtin
        except ConnectionError as err:
            if ssl is True:
                self.set_gitlab(site, ssl=False)
            print(err)
            raise ConnectionAbortedError(f'Ошибка подключения к сайту {site}') from err
        except gitlab.exceptions.GitlabAuthenticationError as err:
            print(err)
            raise gitlab.GitlabAuthenticationError('Ошибка аутентификации') from err
        return self._git

    def set_group(self, group):
        """
        Установка объекта группы для облегчени работы с ним
        Args:
            group: объект группы

        """
        self._group = group

    def set_add_g(self, group, file_prepods, file_users, subgroup=None):
        check_names(group, subgroup, 'group')

        self._group_name = group
        self._subgroup_name = subgroup

        try:
            prepods = pd.read_csv(file_prepods, sep=";")
            users = pd.read_csv(file_users, sep=";")

        except FileNotFoundError as err:
            print(err)
            raise FileNotFoundError(f"Не найден файл {err.filename}") from err
        self._file_prepods = prepods
        self._file_users = users

    def set_del_g(self, group, subgroup=None):
        check_names(group, subgroup, 'group')
        self._group_name = group
        self._subgroup_name = subgroup

    def set_add_s(self, sub_name, branch, file_prepods):
        check_names(branch, sub_name, 'sub')

        self._group_name = branch
        self._subgroup_name = sub_name

        try:
            self._file_prepods = pd.read_csv(file_prepods, sep=";")
        except FileNotFoundError as err:
            print(err)
            raise FileNotFoundError(f"Не найден файл {err.filename}") from err

    def set_del_m(self, group, file_users, subgroup=None):

        check_names(group, subgroup, 'group')

        self._group_name = group
        self._subgroup_name = subgroup
        try:
            self._file_users = pd.read_csv(file_users, sep=";")
        except FileNotFoundError as err:
            print(err)
            raise FileNotFoundError(f"Не найден файл {err.filename}") from err

    def set_add_o(self, milestone, kidp, result, group_id=None):
        if kidp is None:
            raise ValueError("Вы не выбрали, kidp-группа или нет!")
        if bool(int(kidp)) is True:
            self._kidp = True

        if milestone:
            try:
                time.strptime(milestone, '%Y-%m-%d')
                self._milestone = milestone
            except ValueError as err:
                raise ValueError("Формат milestone должен быть 'ГГГГ-ММ-ДД'! ") from err
        if result is None:
            raise ValueError("Вы не выбрали, группа для сдачи предмета или нет!")
        if bool(int(result)) is True:
            self._result = True

        self._id = group_id

    def result_group(self):
        """
        Привязка группы к result
        Returns:

        """
        if self._result is True:
            try:
                group = self._git.groups.list(search='results', order_by='id')[0]
                self._git.groups.create({'name': self._group_name,
                                         'path': self._group_name,
                                         'parent_id': group.id})

            except gitlab.exceptions.GitlabCreateError:
                warnings.warn(f"Подгруппа {self._group_name} уже существует", ResourceWarning)
            except IndexError as err:
                raise IndexError("Группа results не найдена") from err
            return True

        return False

    def get_by_id(self):
        if self._id is not None:
            group = self._git.groups.get(self._id)
            return group
        return False

    def create_proj(self):
        # pylint: disable=invalid-sequence-index
        """
        Создание проектов для заданного списка слушателей, а также
        добавления их в группу и проект

        Warnings:
                UserWarning - ошибка добавления

        """
        for i, j in zip(self._file_users.fullname, self._file_users.login):
            try:
                user = self._git.users.list(username=j)[0]

                # Создаём проект и добавляем в него слушателя
                project = self._git.projects.create({'name': i, 'namespace_id': self._group.id})
                project.members.create({'user_id': user.id,
                                        'access_level': DEVELOPER})

                if self._kidp:
                    self.make_milestone(project, i)
                    project_dev = self._git.projects.create({'name': f'{i}-dev',
                                                             'namespace_id': self._group.id})
                    project_dev.members.create({'user_id': user.id,
                                                'access_level': DEVELOPER})

            except IndexError:
                warnings.warn(f"Пользователь {i} не найден и не добавлен в members"
                              , UserWarning)
            except gitlab.exceptions.GitlabCreateError as err:
                warnings.warn(f"Ошибка создания проекта {i} в группе,"
                              f" {err.args[0]['name'][0]}")

    def get_subgroup(self, group, subgroup_name):
        """
            Получение группы как объекта
            Args:
                group: группа
                subgroup_name: подгруппа

            Returns: Объект группы
        """
        try:
            subgroup = group.subgroups.list(search=subgroup_name)[0]
            self._subgroup = self._git.groups.get(subgroup.id, lazy=True)
        except IndexError as err:
            raise IndexError(f'Не найдена группа {subgroup_name}') from err

        return self._subgroup

    def add_prepods(self):
        """
            Добавление преподавателей на сервис git
        """
        for j in self._file_prepods.login:
            try:
                user = self._git.users.list(username=j)[0]
                self._group.members.create({'user_id': user.id, 'access_level': OWNER})

            except IndexError:
                warnings.warn(f"Преподаватель {j} не найден и не добавлен в members"
                              , UserWarning)
            except gitlab.GitlabCreateError:
                warnings.warn(f"Преподаватель {j} уже добавлен в members"
                              , UserWarning)

    def add_to_group(self, user_id, fullname):
        """
        Добавление слушателей в группу на сервисе git
        с правами GUEST
        Args:
                fullname:       имя пользователя
                user_id:        id пользователя

        """
        try:

            self._group.members.create({'user_id': user_id,
                                        'access_level': GUEST})
        except gitlab.exceptions.GitlabCreateError as err:
            warnings.warn(f"Ошибка добавления {fullname} в группу,  {err.args[0]}")

    # Создание milestone вводится в формате ГГГГ-ММ-ДД
    def make_milestone(self, project, title):
        """
        Создание вехи.
        Args:
            project: Объект проекта
            title:   fullname пользователя

        """
        # project = self._git.projects.list(search=project_name)[0]
        # project.milestones.create({'title': title, 'due_date': due_date})
        try:
            project.milestones.create({'title': title, 'due_date': self._milestone})
        except gitlab.GitlabCreateError as err:
            warnings.warn(f"Ошибка создания milestone в группе {project.name},  {err.args[0]}")

    def delete_users(self, group):
        for user in self._file_users.login:
            try:
                group.members.delete(self._git.users.list(username=user)[0].id)
            except gitlab.exceptions.GitlabDeleteError:
                warnings.warn(f"Пользователя {user} нет в {self._group_name}")
            except IndexError:
                warnings.warn(f"Пользователь {user} не найден")


def check_names(group, subgroup, type_work):
    """
    Проверка правильного ввода перменных
    Args:
        group:      название группы
        subgroup:   название подгруппы
        type_work:  тип функции

    """
    if type_work == 'group':
        if not group:
            raise ValueError("Название группы не может быть пустым! ")
        if re.search(r'[а-яА-Я]', group):
            raise ValueError("Нельзя использовать русские буквы в группах! ")
        if subgroup and re.search(r'[а-яА-Я]', subgroup):
            raise ValueError("Нельзя использовать русские буквы в группах! ")
        return
    if type_work == 'sub':
        if not group or not subgroup:
            raise ValueError("Названия не могут быть пустыми! ")

        if re.search(r'[а-яА-Я]', group) \
                or re.search(r'[а-яА-Я]', subgroup):
            raise ValueError("Нельзя использовать русские буквы в "
                             "предмете или ветке! ")
        return
