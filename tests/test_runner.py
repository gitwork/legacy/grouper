import unittest
import argparse

from tests import test_integrations

if __name__ == '__main__':
    PARSER = argparse.ArgumentParser(description='Test_GitWork_grouper')
    PARSER.add_argument('--token', '-t', type=str)
    PARSER.add_argument('--group_add', action='store_true')
    PARSER.add_argument('--sub_add', action='store_true')
    PARSER.add_argument('--group_del', action='store_true')
    PARSER.add_argument('--group_delm', action='store_true')
    PARSER.add_argument('--args', action='store_true')
    PARSER.add_argument('--all', action='store_true')
    ARGS = PARSER.parse_args()

    if not ARGS.token:
        raise Exception("No token")
    MainTestSuite = unittest.TestSuite()
    test_integrations.get_token(ARGS.token)
    if ARGS.group_add or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestAddGroup))
    if ARGS.group_del or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestDeleteGroup))
    if ARGS.group_del or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestAddSubject))
    if ARGS.group_delm or ARGS.all:
        MainTestSuite.addTest(unittest.makeSuite(test_integrations.TestDeleteMember))

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(MainTestSuite)
