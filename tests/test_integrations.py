"""
Тесты для программы Grouper
"""
import unittest

import gitlab

from scripts.gitlab_connection import Connection
from scripts import methods

# pylint: disable=missing-function-docstring
# TOKEN_ = 'K-_UhP2xtLpAhTc8szG1'
TOKEN_ = 'o39_dp3zEQby2VRbeUXC'


def get_token(token_com):
    # pylint: disable=global-statement
    global TOKEN_
    TOKEN_ = token_com


class TestConnection(unittest.TestCase):
    """
    Тест класса
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_token(TOKEN_)

    def test_connect(self):
        self.assertIsNotNone(self.connection.set_gitlab())

    def test_wrong_token(self):
        wrong_token = 'WrongToken'
        with self.assertRaises(gitlab.GitlabAuthenticationError
                               ) as context:
            self.connection.set_token(wrong_token)
            self.connection.set_gitlab()
        self.assertTrue('Ошибка входа' in str(context.exception))

    def test_wrong_name_g(self):
        group = 'Русский'
        subgroup = None
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        with self.assertRaises(ValueError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue('Нельзя использовать русские '
                        'буквы в группах' in str(context.exception))

    def test_wrong_name_s(self):
        group = 'right'
        subgroup = 'Русский'
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        with self.assertRaises(ValueError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue('Нельзя использовать русские '
                        'буквы в группах' in str(context.exception))

    def test_none_name_s(self):
        group = None
        subgroup = 'Русский'
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        with self.assertRaises(ValueError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue('Название группы не может быть пустым' in str(context.exception))

    def test_wrong_csv(self):
        group = 'ok'
        subgroup = 'ok'
        file_users = "csv_test/test10765_u.csv"
        file_prepods = "csv_test/test18493_p.csv"
        with self.assertRaises(FileNotFoundError
                               ) as context:
            self.connection.set_add_g(group, file_prepods, file_users, subgroup)
        self.assertTrue(f'Не найден файл {file_prepods}' in str(context.exception))

    def check_secure(self):
        with self.assertRaises(UserWarning
                               ) as context:
            methods.check_secure(None, "Right_group123141")
        self.assertTrue(f'Вы не выбрали параметр защищенности группы' in str(context.exception))


class TestAddGroup(unittest.TestCase):
    """
    Тестирование функции group_add.
    Также происходит добавление пользователей и преподавателей в members
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_token(TOKEN_)
        self.connection.set_gitlab()
        self.git = self.connection.ret_git()

    def test_add_wrong_member(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Right_group_Name_2130972"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning
                               ) as context:
            methods.create_group_and_proj(self.connection)

        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(UserWarning)

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_group(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Right_group_Name_84272123"
        self.connection.set_add_g(group_name, file_prepods, file_users)
        with self.assertRaises(Warning
                               ) as context:
            methods.create_group_and_proj(self.connection)
        self.assertTrue('Успешное выполнение' in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_exist_group(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Exist_group_Name_2130484272"
        self.connection.set_add_g(group_name, file_prepods, file_users)

        with self.assertRaises(Warning
                               ) as context:
            methods.create_group_and_proj(self.connection)
            methods.create_group_and_proj(self.connection)
        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(ResourceWarning)

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_sub(self):
        self.setUp()
        file_users = "../csv_test/test_s.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Right_group_Name_2130484272"
        subgroup_name = "Right_subgroup_Name_2130484272"
        self.connection.set_add_g(group_name, file_prepods,
                                  file_users, subgroup_name)
        with self.assertRaises(Warning
                               ) as context:
            methods.create_group_and_proj(self.connection)
        self.assertTrue('Успешное выполнение' in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_add_exist_subgroup(self):
        self.setUp()
        file_users = "../csv_test/test1_u.csv"
        file_prepods = "../csv_test/test1_p.csv"
        group_name = "Exist_group_Name_2130484272"
        subgroup_name = "Right_subgroup_Name_213dfg2"
        self.connection.set_add_g(group_name, file_prepods,
                                  file_users, subgroup_name)

        with self.assertRaises(Warning
                               ) as context:
            methods.create_group_and_proj(self.connection)
        self.assertTrue('Успешное выполнение' in str(context.exception))

        with self.assertRaises(Warning
                               ) as context:
            methods.create_group_and_proj(self.connection)
        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(ResourceWarning)

        group = self.git.groups.list(search=group_name)[0]
        group.delete()


class TestAddSubject(unittest.TestCase):
    """
    Тесты для функции добавления предмета
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_token(TOKEN_)
        self.connection.set_gitlab()
        self.git = self.connection.ret_git()

    def wrong_name(self):
        self.setUp()
        sub_name = 'Wrong_sub_Name_21304000'
        branch = 'Wrong_sub_Name_21304000'
        file_prepods = "../csv_test/test1_p.csv"
        self.connection.set_add_s(sub_name, branch, file_prepods)
        with self.assertRaises(gitlab.GitlabSearchError
                               ) as context:
            methods.create_sub(self.connection)
        self.assertTrue(f"{branch} не найден" in str(context.exception))

    def test_pass_add(self):
        self.setUp()
        sub_name = 'Right_sub_Name_21304872'
        branch = 'Right_branch_Name_21304872'
        file_prepods = "../csv_test/test1_p.csv"

        self.git.groups.create({'name': branch, 'path': branch})

        self.connection.set_add_s(sub_name, branch, file_prepods)
        with self.assertRaises(Warning
                               ) as context:
            methods.create_sub(self.connection)
        self.assertTrue("Успешное выполнение" in str(context.exception))

        group = self.git.groups.list(search=branch)[0]
        group.delete()

    def test_add_exist_subject(self):
        self.setUp()
        sub_name = 'Right_sub_Name_213048354'
        branch = 'Right_branch_Name_21304fgf'
        file_prepods = "../csv_test/test1_p.csv"

        self.connection.set_add_s(sub_name, branch, file_prepods)
        self.git.groups.create({'name': branch, 'path': branch})

        with self.assertRaises(Warning
                               ) as context:
            methods.create_sub(self.connection)
            methods.create_sub(self.connection)
        self.assertTrue('Успешное выполнение' in str(context.exception))
        self.assertWarns(ResourceWarning)

        group = self.git.groups.list(search=branch)[0]
        group.delete()

    def test_wrong_branch(self):
        self.setUp()
        sub_name = 'Right_sub_Name_213048354'
        branch = 'Wrong_branch_Name_285464'
        file_prepods = "../csv_test/test1_p.csv"

        self.connection.set_add_s(sub_name, branch, file_prepods)

        with self.assertRaises(gitlab.GitlabSearchError) as context:
            methods.create_sub(self.connection)
        self.assertTrue(f"{branch} не найден" in str(context.exception))


class TestDeleteGroup(unittest.TestCase):
    """
    Тесты для функции delete_group. Проверены все исключение, успешное выполнение одной операции
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_token(TOKEN_)
        self.connection.set_gitlab()
        self.git = self.connection.ret_git()

    def test_wrong_g_name(self):
        self.setUp()
        group_name = "Wrong_group_Name_7902345346421"
        self.connection.set_del_g(group_name)

        with self.assertRaises(IndexError) as context:
            methods.delete_group(self.connection)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))

    def test_wrong_sg_name(self):
        self.setUp()
        group_name = "Wrong_group_Name_8321041"
        subgroup_name = "Wrong_subgroup_Name_87812421"
        self.connection.set_del_g(group_name, subgroup_name)

        self.git.groups.create({'name': group_name, 'path': group_name})
        with self.assertRaises(IndexError) as context:
            methods.delete_group(self.connection)
        self.assertTrue(f"{subgroup_name} не найден" in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()

    def test_protect_g(self):
        self.setUp()
        group_name = "subjects"
        self.connection.set_del_g(group_name)

        with self.assertRaises(SystemError) as context:
            methods.delete_group(self.connection)
        self.assertTrue('Группа защищена' in str(context.exception))

    def test_pass_del(self):
        self.setUp()
        group_name = "Right_group_Name_73219"
        self.connection.set_del_g(group_name)
        self.git.groups.create({'name': group_name, 'path': group_name})
        with self.assertRaises(Warning) as context:
            methods.delete_group(self.connection)
        self.assertTrue(f"Группа {group_name} удалена успешно" in str(context.exception))

    def test_pass_del_sg(self):
        self.setUp()
        subgroup_name = 'Right_sub_Name_213048354'
        group_name = 'Right_branch_Name_21304fgf'
        file_prepods = "../csv_test/test1_p.csv"
        file_users = "../csv_test/test1_u.csv"

        self.connection.set_add_g(group_name, file_prepods, file_users, subgroup_name)
        self.connection.set_del_g(group_name, subgroup_name)

        with self.assertRaises(Warning
                               ) as context:
            methods.create_group_and_proj(self.connection)
        self.assertTrue("Успешное выполнение" in str(context.exception))

        with self.assertRaises(Warning) as context:
            methods.delete_group(self.connection)
        self.assertTrue(f"Группа {subgroup_name} удалена успешно" in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()


class TestDeleteMember(unittest.TestCase):
    """
    Тесты для функции delete_members. Проверены все исключение, успешное выполнение одной операции
    """
    def setUp(self):
        self.connection = Connection()
        self.connection.set_token(TOKEN_)
        self.connection.set_gitlab()
        self.git = self.connection.ret_git()

    def test_wrong_group(self):
        self.setUp()
        group_name = "Wrong_group_Name_123915"
        file_users = "../csv_test/test_s.csv"
        self.connection.set_del_m(group_name, file_users)

        with self.assertRaises(IndexError) as context:
            methods.delete_members(self.connection)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))

    def test_wrong_subgroup(self):
        self.setUp()
        group_name = "Wrong_group_Name_93972114"
        subgroup_name = "Wrong_subgroup_Name_8345267"
        file_users = "../csv_test/test_s.csv"
        self.connection.set_del_m(group_name, file_users, subgroup_name)

        with self.assertRaises(IndexError) as context:
            methods.delete_members(self.connection)
        self.assertTrue(f"{group_name} не найден" in str(context.exception))

    def test_pass_delete(self):
        self.setUp()
        group_name = "Right_group_Name_93972114"
        file_users = "../csv_test/test_s.csv"

        self.connection.set_del_m(group_name, file_users)
        self.git.groups.create({'name': group_name, 'path': group_name})

        with self.assertRaises(Warning
                               ) as context:
            methods.delete_members(self.connection)
        self.assertTrue(f"Пользователи удалены успешно из {group_name}" in str(context.exception))

        group = self.git.groups.list(search=group_name)[0]
        group.delete()


if __name__ == '__main__':
    unittest.main()
