# Назначение программного средства
 
 Настоящее программное средство позволяет на сервисе *gitlab*: 

 * Создавать группу, подгруппу, проекты в ней с добавлением участников и назначением прав.
   
 * Удалять группу, подгруппу.
   
 * Удалять список слушателей из группы, подгруппы.


# Сборка и запуск 
Установка всех необходимых пакетов

    pip install -r requirements.txt
# Запуск pylint

    pylint main.py - pylint для main
---
    pylint methods.py - pylint для методов
# Запуск тестов

    python test_runner.py <аргументы> --token <токен>

### Возможные аргументы:
    --all - запуск всех тестов
***
    --group_add - запуск тестов по созданию групп и подгрупп
***
    --group_del - запуск тестов по удалению групп и подгрупп
***    
    --group_delm - запуск тестов по удалению пользователей
***
    --sub_add    - запуск тестов по добавлению предмета
# Запуск cli 

    usage: main.py [-h] [-t TOKEN] [-n NAME] [-s SUBNAME] [-u USERS] [-p PREPODS] [-g] [-m MILESTONE] [-i INSIDE] [--group_del] [--sub_add] [--group_delm] [--group_add]


### Опциональные аргументы:

Show this help message and exit

    -h, --help

Input key for work

    -t TOKEN, --token TOKEN

Input name of main group

    -n NAME, --name NAME  

Input name of subgroup

    -s SUBNAME, --subname SUBNAME
                        
Input dir for users

    -u USERS, --users USERS
                        
Input dir for prepods

    -p PREPODS, --prepods PREPODS
                        
Add protect for group

    -g, --guarded         

Add milestones for projects. Format DD-MM-YY

    -m MILESTONE, --milestone MILESTONE

Make a kidp group
    
    -k KIDP, --kidp KIDP
                        
  
    
###Аргументы функций
Delete group

    --group_del           
Delete subgroup

    --group_dels          
Delete members from group/subgroup

    --group_delm          
Add group. May be secured

    --group_add           

Add subject. May be secured

    --sub_add

# Запуск веб-варианта

    python main.py
___
    main.py

# Запуск докер


```bash
   sudo docker build -t grouper:v0.1 .
```

докер образ с параметрами: 

```bash
sudo docker run -it --rm grouper:v0.1 --token <токен> --group_del -n Test_group
```

запуск с пробросом папки/файлов: 

```bash
sudo docker run -v /path/on/host:/path/on/docker --rm grouper:v0.1 -t <токен> <параметры>
```
# Примеры запуска

Добавление группы

    main.py --group_add -t token -n new_group -u ./data/1.csv -p ./data/2.csv

Добавление группы kidp с milestone (файлы пользователей дефолтные)

    main.py --group_add -t token -n group -s new_sub -k -m 2022-12-01

Удаление группы

    main.py --group_del -t token -n old_group

Удаление пользователей из подгруппы

    main.py --group_delm -t token -n old_group -s old_subgroup

Добавление предмета

    main.py --sub_add -t token -n branch -s sub_name
